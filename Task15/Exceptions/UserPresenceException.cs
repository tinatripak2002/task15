﻿using System;
using System.Runtime.Serialization;

namespace Task15.Exceptions
{
    [Serializable]
    public class UserPresenceException : Exception
    {
        public UserPresenceException()
        {
        }

        public UserPresenceException(string message) : base(message)
        {
        }

        public UserPresenceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UserPresenceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
