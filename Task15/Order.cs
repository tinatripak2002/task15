﻿namespace Task15
{
    public class Order
    {
        public enum Statuses
        {
            New,
            CanceledByAdministrator,
            PaymentReceived,
            Sent,
            Received,
            Completed,
            CanceledByUser
        }
        public Product Product { get; set; }
        public Statuses Status { get; set; }
        public bool IsPaid { get; set; } = false;
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public int NewPostOffice { get; set; }
        public Order() { }
        public Order(Product product, string phoneNumber, string city, int newPostOffice)
        {
            Product = product;
            Status = Statuses.New;
            PhoneNumber = phoneNumber;
            City = city;
            NewPostOffice = newPostOffice;
        }
        public override string ToString()
        {
            if (IsPaid) return $"{Product} | Status: {Status,25} | IsPaid: No";
            else return $"{Product} | Status: {Status,25} | IsPaid: Yes";
        }
    }
}
