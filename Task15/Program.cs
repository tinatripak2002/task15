﻿using System;
using System.Collections.Generic;
using Task15.Roles;
using Task15.DB;

namespace Task15
{
    public class Program : DBUsers
    {
        private static User myUser;
        static readonly Dictionary<int, Action> Actions = new Dictionary<int, Action>();
        static readonly Dictionary<int, Func<User>> SetDict = new Dictionary<int, Func<User>>();
        static void Main()
        {
            Console.OutputEncoding = System.Text.Encoding.Default;

            AddProducts();
            myUser = new Admin();
            AddDictionaryAdmin();
            myUser = new RegisteredUser();
            AddDictionaryRegisteredUser();
            myUser = new Guest();
            AddDictionary();
            AddUsers();
            Console.WriteLine("Write the number that corresponds to the desired option for you: \n" +
                "1. Show products\n" +
                "2. Show my basket\n" +
                "3. Show orders of users for Admin\n" +
                "4. Add or no add to basket\n" +
                "5. Create order\n" +
                "6. Add product for Admin\n" +
                "7. Exit\n" +
                "8. View history\n" +
                "9. Show info about user for Admin");
            int key = Convert.ToInt32(Console.ReadLine());

            while (key != 0)
            {
                if (key < 5)
                {
                    Console.OutputEncoding = System.Text.Encoding.Default;

                    Actions[key]();
                }
                else
                {
                    if (myUser is Guest)
                    {
                        myUser = SetDict[key]();
                    }
                    else
                    {
                        if (key == 7)
                            myUser = SetDict[key]();
                        Actions[key]();

                    }
                }
                Console.WriteLine("Write again the number that corresponds to the desired option for you");
                key = Convert.ToInt32(Console.ReadLine());
            }
            static void AddDictionary()
            {
                Actions.Add(1, myUser.ShowProducts);
                Actions.Add(2, myUser.ShowBasket);
                Actions.Add(4, myUser.AddOrNoAddToBasket);
                SetDict.Add(5, (myUser as Guest).Logination);
                SetDict.Add(6, (myUser as Guest).Registration);
                SetDict.Add(7, myUser.Exit);
                Actions.Add(5, myUser.CreateOrder);
            }

            static void AddDictionaryAdmin()
            {
                Actions.Add(3, (myUser as Admin).ShowOrdersOfUsers);
                Actions.Add(6, (myUser as Admin).AddProduct);
                Actions.Add(9, (myUser as Admin).ShowInfoAboutUsers);
            }

            static void AddDictionaryRegisteredUser()
            {
                Actions.Add(8, (myUser as RegisteredUser).ViewHistory);
            }
            static void AddUsers()
            {
                Users.Add(new Admin("Kristina", "Tripak", "kristina2002", "qwertyqwerty", 1234567890, 1000));
                Users.Add(new Admin("Nika", "Santo", "veronika_santo", "nika00001", 1289854991, 5000));
                Users.Add(new RegisteredUser("Danil", "Acro", "acro_danil", "xorxordanil", 1968306891, 789));
                Users.Add(new RegisteredUser("Denis", "Radchenko", "den_radchenko", "raddenko2002", 2069418573, 1780));
            }
            static void AddProducts()
            {
                DBProducts.AddProduct(new Product("Golden apple", "Fruit", "Riola", 100));
                DBProducts.AddProduct(new Product("Grapes", "Fruit", "GraFruit", 80));
                DBProducts.AddProduct(new Product("Potatos", "Vegetables", "Pot", 44));
                DBProducts.AddProduct(new Product("Tomatos", "Vegetables", "TomatFredy", 49));
                DBProducts.AddProduct(new Product("Avocado", "Fruit", "Avo", 35));
                DBProducts.AddProduct(new Product("Bananas", "Fruit", "Bananas", 15));
                DBProducts.AddProduct(new Product("Bread with cranberries", "Bakery", "Bakery", 40));
                DBProducts.AddProduct(new Product("Cinnabon pie", "Bakery", "Bakery", 35));
                DBProducts.AddProduct(new Product("Pizza four cheeses", "Bakery", "Chelentano", 120));
                DBProducts.AddProduct(new Product("White yogurt", "Milk products", "Danone", 20));
                DBProducts.AddProduct(new Product("Kinder Choco", "Candies", "Kinder", 55));
                DBProducts.AddProduct(new Product("Chocolate with nuts", "Candies", "Roshen", 29));
                DBProducts.AddProduct(new Product("Maria cookies", "Cookies", "Roshen", 16));
                DBProducts.AddProduct(new Product("Strawberry", "Fruit", "Swettyfruits", 69));
                DBProducts.AddProduct(new Product("Orange", "Fruit", "Redfriends", 82));
                DBProducts.AddProduct(new Product("Cake", "Bakery", "Roshen", 250));
                DBProducts.AddProduct(new Product("Cranberry", "Fruit", "Cranfruits", 78));

            }
        }
    }
}
