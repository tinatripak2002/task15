﻿using Task15.Roles;
using System.Collections.Generic;
using System.Linq;

namespace Task15.DB
{
    public class DBUsers 
    {
        public static List<User> Users { get; set; } = new List<User>();
        public void AddUser(User user)
        {
            foreach (var user1 in Users)
            {
                if (user1.Login == user.Login || user1.CardNumber == user.CardNumber)
                {
                    return;
                }
            }
            Users.Add(user);
        }
        public void RemoveUser(User user)
        {
            if (Users.Contains(user)) Users.Remove(user);
        }
        public List<User> ShowListOfUsers()
        {
            return Users.OrderBy(x => x.LastName).ToList();
        }
        public bool FindLoginExists(string login)
        {
            foreach (User item in Users)
            {
                if (item.Login == login)
                {
                    return true;
                }
            }
            return false;
        }
        public User FindLogin(string login)
        {
            foreach (User item in Users)
            {
                if (item.Login == login)
                {
                    return item;
                }
            }
            return null;
        }
    }
}
