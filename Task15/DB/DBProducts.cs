﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task15.DB
{
    public static class DBProducts
    {
        public static List<Product> Products { get; set; } = new List<Product>();
        public static void AddProduct(Product product)
        {
            if (!Products.Contains(product)) Products.Add(product);
        }
        public static void RemoveProduct(Product product)
        {
            if (Products.Contains(product)) Products.Remove(product);
        }
        public static Product FindProduct(string name)
        {
            foreach (var product in Products)
            {
                if (string.Compare(product.Name, name, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    return product;
                }
            }
            return null;
        }
        public static List<Product> ShowListOfProducts()
        {
            return Products.OrderBy(x => x.Name).ToList();
        }
    }
}
