﻿using System;
using System.Collections.Generic;

namespace Task15
{
    public class History
    {
        public List<Product> Products { get; set; } = new List<Product>();
        public DateTime Date { get; set; }
        public History() { }
        public History(List<Product> products, DateTime date)
        {
            Products = products;
            Date = date;
        }
    }
}
