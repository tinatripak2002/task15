﻿using System;
using Task15.Roles.Interfaces;

namespace Task15.Roles
{
    public class RegisteredUser : User, IRegisteredUser
    {
        public RegisteredUser() { }
        public RegisteredUser(string firstname, string lastname, string login, string password)
            : base(firstname, lastname, login, password) { }
        public RegisteredUser(string firstname, string lastname, string login, string password, long cardnumber, decimal money)
            : base(firstname, lastname, login, password, cardnumber, money) { }

        public void CancelOrder(Order order)
        {
            if (order.Status != Order.Statuses.Received
                && order.Status != Order.Statuses.Completed
                && order.Status != Order.Statuses.CanceledByAdministrator)
            {
                order.Status = Order.Statuses.CanceledByUser;
            }
        }

        public void SetStatusReceived(Order order)
        {
            order.Status = Order.Statuses.Received;
        }


        public void ViewHistory()
        {
            foreach (var item in Histories)
            {
                string a = string.Join(",", item.Products);
                Console.WriteLine($"Products: {a}\nData: {item.Date}");
            }
        }
    }
}
