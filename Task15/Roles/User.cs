﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task15.Roles.Interfaces;
using Task15.DB;

namespace Task15.Roles
{
    public class User : DBUsers, IUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public long CardNumber { get; set; }
        public decimal Money { get; set; }
        public List<Order> Orders { get; set; } = new List<Order>();
        public List<Product> BasketOfProducts { get; set; } = new List<Product>();
        public List<History> Histories { get; set; } = new List<History>();

        public User()
        { }
        public User(string firstname, string lastname, string login, string password)
        {
            FirstName = firstname;
            LastName = lastname;
            Login = login;
            Password = password;

        }
        public User(string firstname, string lastname, string login, string password, long cardnumber, decimal money)
            : this(firstname, lastname, login, password)
        {
            CardNumber = cardnumber;
            Money = money;
        }
        public void AddToBasket(Product product)
        {
            BasketOfProducts.Add(product);
        }
        public void RemoveFromBasket(string item)
        {
            Product product = FindProduct(item);
            BasketOfProducts.Remove(product);
        }
        public void AddOrNoAddToBasket()
        {
            Console.WriteLine("Enter the product which you want to find:");
            var item = Console.ReadLine();
            Product product = FindProduct(item);
            Console.WriteLine($"Do you want to add {product} to the basket?\n" +
                    "1. Yes \n" +
                    "2. No");
            if (Console.ReadLine() == "1")
            {
                AddToBasket(product);
            }
            else Console.WriteLine("Okay, let's continue");
        }
        public void ShowBasket()
        {
            int i = 1;
            if (BasketOfProducts.Count != 0)
            {
                foreach (var item in BasketOfProducts)
                {
                    Console.WriteLine($"{i} - {item}");
                    i++;
                }
            }
        }
        public Product FindProduct(string name)
        {
            Product product = DBProducts.FindProduct(name);
            return product;
        }
        public void ShowInfoAboutUsers()
        {
            List<User> users = ShowListOfUsers();
            foreach (var item in users)
            {
                Console.WriteLine(item);
            }
        }

        public void ShowProducts()
        {
            List<Product> products = DBProducts.ShowListOfProducts();
            foreach (var item in products)
            {
                Console.WriteLine(item);
            }
        }
        public void CreateOrder()
        {
            Console.WriteLine("This order is yourself");
            ShowBasket();
            Console.Write("Maybe you want to add or remove smth?\nPress:\nY if you want or N if no\n");
            var choose = Console.ReadKey().Key;
            while (choose == ConsoleKey.Y)
            {
                Console.Write("\nR for remove and A for add smth\n");
                var key = Console.ReadKey().Key;
                if (key == ConsoleKey.A)
                {
                    AddOrNoAddToBasket();
                }
                else if (key == ConsoleKey.R)
                {
                    Console.Write("\nWrite what you want to remove:");
                    string item = Console.ReadLine();
                    RemoveFromBasket(item);
                }
                ShowBasket();
                Console.Write("Maybe you want to add or remove smth?\nPress:\nY if you want or N if no\n");
                choose = Console.ReadKey().Key;
            }
            Console.WriteLine("Write your phone number");
            string phone = Console.ReadLine();
            Console.WriteLine("Write a number of new post office");
            int number = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Write your city");
            string city = Console.ReadLine();
            decimal sum = BasketOfProducts.Sum(x => x.Price);
            Console.WriteLine($"Sum of this order is {sum} ₴");
            if (!(Users.Select(x => x) is Guest))
            {
                if (Money < sum)
                {
                    Console.WriteLine("You cannot buy all these products");
                }
                else
                {
                    foreach (var item in BasketOfProducts)
                    {
                        CreateOrder(new Order(item, phone, city, number));
                    }
                }
                Histories.Add(new History { Products = BasketOfProducts, Date = DateTime.Now });

            }
        }
        public void CreateOrder(Order order)
        {
            BasketOfProducts.Remove(order.Product);
            Orders.Add(order);
        }
        public void EditPhoneInOrder(Order order, string phone) => order.PhoneNumber = phone;
        public void EditCityInOrder(Order order, string city) => order.City = city;
        public void EditPostOfficeInOrder(Order order, int number) => order.NewPostOffice = number;
        public void EditOrder(Order order, string phone, int number, string city)
        {
            order.City = city;
            order.NewPostOffice = number;
            order.PhoneNumber = phone;
        }
        public void ChangeUsersCardNumber(long cardnumber)
        {
            CardNumber = cardnumber;
            Console.WriteLine($"Card number has been changed to {cardnumber}");
        }

        public void ChangeUsersLogin(string login)
        {
            Login = login;
            Console.WriteLine($"Login has been changed to {login}");
        }

        public void ChangeUsersInfo(string firstname, string lastname)
        {
            FirstName = firstname;
            LastName = lastname;
            Console.WriteLine($"First name has been changed to {firstname} and last name to {lastname}");
        }

        public void ChangeUsersMoney(decimal money)
        {
            Money = money;
            Console.WriteLine($"Money has been changed to {money}");
        }

        public void ChangeUsersPassword(string password)
        {
            Password = password;
            Console.WriteLine($"Password has been changed to {password}");
        }

        public Guest Exit()
        {
            return new Guest();
        }
        public override string ToString()
        {
            return $"{FirstName,10}|{LastName,10}|{Login,15}|{Password,10}";
        }
    }
}
