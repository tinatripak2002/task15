﻿namespace Task15.Roles.Interfaces
{
    interface IUser
    {
        void AddToBasket(Product product);
        void RemoveFromBasket(string item);
        void ShowBasket();
        void AddOrNoAddToBasket();
        Product FindProduct(string name);
        void ShowInfoAboutUsers();
        void ShowProducts();
        void CreateOrder();
        void EditPhoneInOrder(Order order, string phone);
        void EditCityInOrder(Order order, string city);
        void EditPostOfficeInOrder(Order order, int number);
        void EditOrder(Order order, string phone, int number, string city);
        void ChangeUsersInfo(string firstname, string lastname);
        void ChangeUsersPassword(string password);
        void ChangeUsersLogin(string login);
        void ChangeUsersMoney(decimal money);
        void ChangeUsersCardNumber(long cardnumber);
        Guest Exit();

    }
}
