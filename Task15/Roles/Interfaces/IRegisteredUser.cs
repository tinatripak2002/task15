﻿namespace Task15.Roles.Interfaces
{
    interface IRegisteredUser
    {
        void SetStatusReceived(Order order);
        void CancelOrder(Order order);
        void ViewHistory();

    }
}
