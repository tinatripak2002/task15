﻿using static Task15.Order;

namespace Task15.Roles.Interfaces
{
    interface IAdmin 
    {
        void AddProduct();

        void ChangeProduct(Product product, string name, string category, string description);
        void ChangeProduct(Product product, decimal price);
        void ChangeStatus(Order order, Statuses status);
        void ShowOrdersOfUsers();
    }
}
