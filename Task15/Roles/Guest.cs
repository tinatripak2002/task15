﻿using System;
using System.Text.RegularExpressions;
using Task15.Roles.Interfaces;
using Task15.Exceptions;

namespace Task15.Roles
{
    public class Guest : User, IGuest
    {
        public User Registration()
        {
            string firstName, lastName, login, password;
            long cardNumber;
            decimal money;
            RegisteredUser registeredUser;
            Console.WriteLine("Please, write your first name:");
            firstName = Console.ReadLine();
            while (!Regex.IsMatch(firstName, @"^[a-zA-Z]+$"))
            {
                Console.WriteLine("Write you first name like 'Vladislav'");
                firstName = Console.ReadLine();
            }
            Console.WriteLine("Please, write your last name:");
            lastName = Console.ReadLine();
            while (!Regex.IsMatch(lastName, @"^[a-zA-Z]+$"))
            {
                Console.WriteLine("Write you first name like 'Vladislav'");
                lastName = Console.ReadLine();
            }
            Console.WriteLine("Please, write the login for you account:");
            login = Console.ReadLine();
            while (FindLoginExists(login))
            {
                Console.WriteLine("Please, select another login beacause this login exists");
                login = Console.ReadLine();
            }
            Console.WriteLine("Please, write the password for you account:");
            password = Console.ReadLine();
            while (password.Length < 8)
            {
                Console.WriteLine("Please, select password where count of chars is more than 8");
                password = Console.ReadLine();
            }
            Console.WriteLine("Please, write the card number for you account:");
            cardNumber = Convert.ToInt32(Console.ReadLine());
            Random x = new Random();
            money = Convert.ToDecimal(x.Next(500, 3000));
            Console.WriteLine("The registration was successful");
            registeredUser = new RegisteredUser(firstName, lastName, login, password, cardNumber, money);
            return RegistrUser(registeredUser);
        }

        public User RegistrUser(RegisteredUser user)
        {
            Users.Add(user);
            return user;
        }

        public User Logination()
        {
            string login, password;
            Console.WriteLine("Please, write your login");
            login = Console.ReadLine();
            Console.WriteLine("Please, write your password");
            password = Console.ReadLine();
            if (FindLoginExists(login))
            {
                return LoginUser(login, password);
            }
            else throw new UserPresenceException("Incorect email");
        }
        public User LoginUser(string login, string password)
        {
            User user = FindLogin(login);
            if (user.Password == password)
            {
                Console.WriteLine("Your login and password is correct. Welcome:)");
                return user;
            }
            else
            {
                throw new UserPresenceException("Incorect password");
            }
        }
    }
}
