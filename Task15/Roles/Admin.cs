﻿using Task15.DB;
using System;
using static Task15.Order;
using Task15.Roles.Interfaces;

namespace Task15.Roles
{
    public class Admin : User, IAdmin
    {
        public Admin() { }
        public Admin(string firstname, string lastname, string login, string password)
            : base(firstname, lastname, login, password) { }
        public Admin(string firstname, string lastname, string login, string password, long cardnumber, decimal money)
            : base(firstname, lastname, login, password, cardnumber, money) { }

        public void ChangeProduct(Product product, string name, string category, string description)
        {
            product.Name = name;
            product.Category = category;
            product.Description = description;
        }

        public void ChangeProduct(Product product, decimal price) => product.Price = price;

        public void ChangeStatus(Order order, Statuses status) => order.Status = status;


        public void AddProduct()
        {
            decimal price;
            string name, description, category;
            Console.WriteLine("Name: ");
            name = Console.ReadLine();
            Console.WriteLine("Category: ");
            category = Console.ReadLine();
            Console.WriteLine("Description: ");
            description = Console.ReadLine();
            Console.WriteLine("Price: ");
            price = Convert.ToDecimal(Console.ReadLine());
            DBProducts.AddProduct(new Product(name, category, description, price));
        }

        public void ShowOrdersOfUsers()
        {
            foreach (User item in Users)
            {
                Console.WriteLine($"User: {item.FirstName} {item.LastName}");
                item.ShowBasket();
            }
        }
    }
}
