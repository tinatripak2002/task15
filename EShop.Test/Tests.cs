using NUnit.Framework;
using Task15.DB;
using Task15.Roles;
using System.Linq;

namespace Task15.Test
{
    public class Tests
    {
        [Test]
        public void AddUser_TestForAdmin()
        {
            Admin admin = new Admin();
            DBUsers.Users.Add(admin);
            Assert.AreEqual(admin, DBUsers.Users.Last());
        }

        [Test]
        public void AddUser_TestForRegisteredUser()
        {
            RegisteredUser user = new RegisteredUser();
            DBUsers.Users.Add(user);
            Assert.AreEqual(user, DBUsers.Users.Last());
        }
        [Test]
        public void AddToBasket_TestForRegisteredUser()
        {
            RegisteredUser user = new RegisteredUser();
            Product product = new Product();
            user.AddToBasket(product);
            Assert.AreEqual(product, user.BasketOfProducts.Last());
        }
        [Test]
        public void AddToBasket_TestForAdmin()
        {
            Admin admin = new Admin();
            Product product = new Product();
            admin.AddToBasket(product);
            Assert.AreEqual(product, admin.BasketOfProducts.Last());
        }
        [Test]
        public void CreateOrder_TestForAdmin()
        {
            Admin admin = new Admin();
            Product product = new Product();

            admin.AddToBasket(product);
            Order order = new Order(product, "0997716295", "Kiev", 12);
            admin.CreateOrder(order);

            Assert.AreEqual(order, admin.Orders.Last());
        }
        [Test]
        public void SetUp_Test()
        {
            RegisteredUser user = new RegisteredUser("Qwerty", "Yuio", "qwerty@gmail.com", "12345", 12345698, 900);
            User guest = new Guest();

            guest = (guest as Guest).RegistrUser(user);

            Assert.That(guest is RegisteredUser);
        }
        [Test]
        public void SetIn_Test()
        {
            RegisteredUser user = new RegisteredUser("Qwerty", "Yuio", "qwerty@gmail.com", "12345", 12345698, 850);
            User guest = new Guest();

            (guest as Guest).RegistrUser(user);
            guest = (guest as Guest).LoginUser("qwerty@gmail.com", "12345");

            Assert.That(guest is RegisteredUser);
        }
        [Test]
        public void CancelOrder_Test()
        {
            RegisteredUser user = new RegisteredUser();
            Product product = new Product();

            user.AddToBasket(product);
            Order order = new Order(product, "0997716295", "Kiev", 12);
            user.CreateOrder(order);
            user.CancelOrder(order);

            Assert.That(user.Orders[user.Orders.IndexOf(order)].Status == Order.Statuses.CanceledByUser);
        }
        [Test]
        public void ChangeEmail_Test()
        {
            RegisteredUser user = new RegisteredUser();
            user.ChangeUsersLogin("ytrewq@gmail.com");

            Assert.That(user.Login == "ytrewq@gmail.com");
        }
        [Test]
        public void ChangePassword_Test()
        {
            RegisteredUser user = new RegisteredUser();
            user.ChangeUsersPassword("89565656");

            Assert.That(user.Password == "89565656");
        }
        [Test]
        public void ChangeFullName_Test()
        {
            RegisteredUser user = new RegisteredUser();
            user.ChangeUsersInfo("Kristina","Rudenko");

            Assert.That(user.FirstName == "Kristina" && user.LastName == "Rudenko");
        }
        [Test]
        public void SetGetToOrder_Test()
        {
            RegisteredUser user = new RegisteredUser();
            Product product = new Product();

            user.AddToBasket(product);
            Order order = new Order(product, "0997716295", "Kiev", 12);
            user.CreateOrder(order);
            user.SetStatusReceived(order);

            Assert.That(user.Orders[user.Orders.IndexOf(order)].Status == Order.Statuses.Received);
        }

        [TestCase(Order.Statuses.CanceledByAdministrator)]
        [TestCase(Order.Statuses.Sent)]
        [TestCase(Order.Statuses.Completed)]
        public void ChangeStatus_Test(Order.Statuses status)
        {
            Admin admin = new Admin();
            Product product = new Product();

            admin.AddToBasket(product);
            Order order = new Order(product, "0997716295", "Kiev", 12);
            admin.CreateOrder(order);
            admin.ChangeStatus(order,status);

            Assert.That(admin.Orders[admin.Orders.IndexOf(order)].Status == status);
        }

        [Test]
        public void ChangeProduct_Test()
        {
            Admin admin = new Admin();
            Product product = new Product();

            admin.ChangeProduct(product, 96);
            Assert.That(product.Price == 96);
        }
    }
}